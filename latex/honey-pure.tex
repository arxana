
\documentclass{article}
\input{arxana-header.tex}

\begin{document}
\title{Pure Honey}

\author{Joseph Corneli \& Raymond Puzio\thanks{Copyright (C) 2017
    Joseph Corneli {\tt <holtzermann17@gmail.com>}\newline
    Copyright (C) 2017 Raymond Puzio {\tt <rsp@planetmath.info>}}}
\date{Last revised: \today}

\maketitle

\abstract{This is a reference implementation of arxana backend
  primitives as described in the honey spec.}

\tableofcontents

\section{Introduction}

\begin{notate}{What we are doing?}
  Here we present a purely functional implementation of nemata and
  plexi as s-expressions.  To make everything explict, we pass the
  plexus as an argument to to our functions and, in the case of
  functions that can modify it, get the plexus back in the output.
  
  In order to make for a reader-friendly exposition we will develop
  our code incrementally.  Since it is meant primarily to illustrate
  principles, the first version of the code presented here will
  hopelessly clunky and inefficient.  In successive versions, we will
  make various opimizations and improvements until we arrive at a more
  practical implementation.
\end{notate}

\begin{notate}{Theoretical definition}
  We begin with a theoretical definition of a plexus as quintuplet $(U,
  G, H, T, C)$ where

  \begin{itemize}
  \item $U$ is a (finite) set of identifiers
  \item $G$ is a distinguished element of $U$. (the ground)
  \item $H$ and $T$ are functions from $U$ to $U$ (head and tail)
  \item $C$ is a function from $U$ to a content set $S$
  \end{itemize}
    
  subject to the conditions that $H(G) = T(G) = G$.

  By using the canonical isomorphism between a set of functions with a
  common domain and a single function from the same domain to the
  product of their codomains, we may rewrite the definition as a
  triplet $(U, G, N)$ where $N$ is a function from $U$ to $H \times T
  \times C$.  The graph of $N$ consists of a set of quartiplets, which
  we call nemata.

  For the purpose of implementing this structure in LISP, we will take
  the identifiers which lie in $U$ to be natural numbers, take $G$ to
  be 0, and take the content set $S$ to be the set of LISP lists.  We
  will represent a nema by cons'ing an element $x$ of $U$ and the
  corresponding values $H(x)$ and $T(x)$ onto $C(x)$.  Since we fixed
  the value of $G$ once and for all, we don't have to restate it, so a
  plexus will be represented as a list whose \verb|car| is $U$ and
  whose \verb|cdr| is the association list of nemata.
\end{notate}

\section{Version 0}

\begin{notate}{About version 0}
  The honey primitives can be described as mathematical functions on
  the structure described above.  We will now write down these
  functions explicitly and translate the mathematical notation into
  LISP to produce the initial version.
\end{notate}

\subsection{Utility routines}

\begin{notate}{Why utilities?}
  Before starting, we need to defun a few simple utility functions.
  Adhering to the highest standards of purity, we write them
  recursively and immutably.
\end{notate}

\begin{notate}{On `next-available-number'}
  The first function takes a set of integers and a starting number
  finds the smallest integer which is not in the list but above the
  starting point.  This will be used to generate identifiers when
  adding new nemata.
\end{notate}
\begin{elisp}
(defun next-available-number (ids start)
  (if (member start ids)
      (next-available-number ids (+ start 1))
    start))
\end{elisp}

\begin{notate}{On `filter'}
  Select out the elements of the list which satisfy the predicate.
\end{notate}
\begin{elisp}
(defun filter (prd lis)
  (if (consp lis)
      (if (funcall prd (car lis))
	  (cons (car lis)
		(filter prd (cdr lis)))
	(filter prd (cdr lis)))
    nil))
\end{elisp}

\begin{notate}{On `cons*'}
  Convenience function which \verb|cons|'es a whole bunch of stuff in
  front of a list.  This could just as well be done as
  \verb(append (list ... ))\verb but since we will be doing this
  fairly often, it tidies up the code to give it a name.
\end{notate}
\begin{elisp}
(defun cons* (&rest args)
  (if (cdr args)
      (cons (car args)
	    (apply 'cons* (cdr args)))
    (car args)))
\end{elisp}

\subsection{Individual Operations}

\begin{notate}{On `add-nema'}
  Formally, we may define $\mathrm{add-nema} : \mathbf{Plex} \times
  \mathbb{N} \times \mathbb{N} \times S \to \mathbf{Plex}$ as
  $((U, G, H, T, C), h, t, c) \mapsto (U' G' H' T' C')$
  where
  \begin{align*}
    U' &= \{0\} \sqcup U \\
    G' &= \mathrm{in}_2 (G) \\
    H' \circ \mathrm{in}_2 &= H, &  H'(\mathrm{in}_1(0)) &= h \\
    T' \circ \mathrm{in}_2 &= T, &  T'(\mathrm{in}_1(0)) &= t \\
    C' \circ \mathrm{in}_2 &= C, &  C'(\mathrm{in}_1(0)) &= c  .
  \end{align*}
  We will implement the coproduct with a singleton by picking a number
  which is not in $U$ and consing it on to $U$.  Then $\mathrm{in}_2$
  is just restriction of $U'$ to $U$ so all we need to do is add a
  nema.
\end{notate}
\begin{elisp}
(defun pure-add-nema-v0 (plex src txt snk)
  (let ((next (next-available-number (car plex) 1)))
    (cons*
     ;; Start with U',
     (cons next (car plex))
     ;; then the the new nema
     (cons* next src snk txt)))
  ;; and end with the old nemata.
  (cdr plex))))
\end{elisp}

\begin{notate}{On `remove-nema'}
  Formally, we may define
  \begin{align*}
    \mathtt{remove{\mhyphen}nema} : \prod_{p
    \colon \mathbf{Plex}} \mathrm{pr}_1 (p) \setminus \{ \mathrm{pr}_2
    (p) \} &\to \mathbf{Plex} \\
    ((U, G, H, T, C), n) &\mapsto (U', G, H
    |_{U'}, T |_{U'}, C |_{U'})
  \end{align*}
  where $U' = U \setminus \{n\}$.
\end{notate}
\begin{elisp}
(defun pure-remove-nema-v0 (plex nema)
  (cons
   (remove nema (car plex))
   (remove (assoc nema (cdr plex))
	   (cdr plex))))
\end{elisp}

\begin{notate}{On `get-src', `get-sink' and `get-content'}
  Formally, we have
  \begin{align*}
    \mathrm{get-source} \colon
      \prod_{p \colon \mathbf{Plex}} \mathrm{pr}_1(p) &\to \mathbb{N} \\
    ((U, G, H, T, C), n) &\mapsto H(n) \\
    \mathrm{get-sink} \colon
      \prod_{p \colon \mathbf{Plex}} \mathrm{pr}_1(p) &\to \mathbb{N} \\
    ((U, G, H, T, C), n) &\mapsto T(n) \\
    \mathrm{get-content} \colon
       \prod_{p \colon \mathbf{Plex}} \mathrm{pr}_1(p) &\to S \\
    ((U, G, H, T, C), n) &\mapsto C(n)
  \end{align*}
Woo-hoo, dependent types!!
\end{notate}
\begin{elisp}
(defun pure-get-source-v0 (plex nema)
  (nthcdr 1 (assoc nema (cdr plex))))

(defun pure-get-sink-v0 (plex nema)
  (nthcdr 2 (assoc nema (cdr plex))))

(defun pure-get-content-v0 (plex nema)
  (nthcdr 3 (assoc nema (cdr plex))))
\end{elisp}

\begin{notate}{On `get-forward-links' and `get-backward-links'}
  Formally,
  \begin{align*}
    \mathrm{get-forward-links} \colon
      \prod_{p : \mathbf{Plex}} \mathrm{pr}_1(p) &\to
      \mathcal{P}(\mathbb{Z}) \\
    ((U, G, H, T, C), n) &\mapsto H^{-1} (n) \\
    \mathrm{get-backward-links} :
      \prod_{p : \mathbf{Plex}} \mathrm{pr}_1(p) &\to
      \mathcal{P}(\mathbb{Z}) \\
  ((U, G, H, T, C) n) &\mapsto T^{-1} (n) .
  \end{align*}
\end{notate}
\begin{elisp}
(defun pure-get-forward-links-v0 (plex nema)
  (mapcar 'car
	  (filter (lambda (x) (equal x (nth 1 nema)))
		  (cdr plex))))

(defun pure-get-backward-links-v0 (plex nema)
  (mapcar 'car
	  (filter (lambda (x) (equal x (nth 2 nema)))
		  (cdr plex))))  
\end{elisp}

\begin{notate}{On `update-source', `update-sink', and
    `update-content'}
  Formally, we have the following:
  \begin{align*}
    \mathtt{update-source} :
    \prod_{p : \mathbf{Plex}}
      \mathrm{pr}_1(p) \times \mathrm{pr}_1(p)
      &\to \mathbf{Plex} \\
    ((U, G, H, T, C), n, m) &\mapsto (U, G, H', T, C)
  \end{align*}
  where $H'(k) = H(k)$ when $k \neq n$ and $H'(n) = m$. 
  \begin{align*}
    \mathtt{update-sink} :
    \prod_{p : \mathbf{Plex}}
      \mathrm{pr}_1(p) \times \mathrm{pr}_1(p)
      &\to \mathbf{Plex} \\
    ((U, G, H, T, C), n, m) &\mapsto (U, G, H, T', C)
  \end{align*}
  where $T'(k) = T(k)$ when $k \neq n$ and $T'(n) = m$.
    \begin{align*}
    \mathtt{update-source} :
    \prod_{p : \mathbf{Plex}}
      \mathrm{pr}_1(p) \times S
      &\to \mathbf{Plex} \\
    ((U, G, H, T, C), n, m) &\mapsto (U, G, H, T, C')
  \end{align*}
  where $C'(k) = C(k)$ when $k \neq n$ and $C'(n) = m$.
\end{notate}

\begin{elisp}
(defun update-source (plex nema new-src)
  (let ((changer (assoc nema (cdr plex))))
    (cons*
     ;; U
     (car plex)
     ;; The nema with an updated source.
     (cons*
      (nth 0 changer)
      new-src
      (nth 2 changer)
      (nthcdr 2 changer))
     ;; The rest of the nemata which remain unchanged.
     (remove changer (cdr plex)))))

(defun update-sink (plex nema new-snk)
  (let ((changer (assoc nema (cdr plex))))
    (cons*
     ;; U
     (car plex)
     ;; The nema with an updated sink.
     (cons*
      (nth 0 changer)
      (nth 1 changer)
      new-snk
      (nthcdr 2 changer))
     ;; The rest of the nemata which remain unchanged.
     (remove changer (cdr plex)))))

(defun update-content (plex nema txt)
  (let ((changer (assoc nema (cdr plex))))
    (cons*
     ;; U
     (car plex)
     ;; The nema with a updated content.
     (cons*
      (nth 0 changer)
      (nth 1 changer)
      (nth 2 changer)
      txt)
     ;; The rest of the nemata which remain unchanged.
     (remove changer (cdr plex)))))
\end{elisp}

\end{document}
