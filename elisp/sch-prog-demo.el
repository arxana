;; sch-prog-demo.el - Minimal example of scholiumific programming

;; Copyright (C) 2013 Raymond S. Puzio

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; COMMENTARY:

;; See http://arxana.net/foo-goo.jpg for a vital map.

;;; CODE:

(load-file "bootstart.el")

(defun filter (pred stuff)
  (let ((ans nil))
    (dolist (item stuff (reverse ans))
      (if (funcall pred item)
	  (setq ans (cons item ans))
	nil))))

;; Make a new scholium-based document.

(defun gen-gnd ()
 '(1 (0 (0) () (0))))

(setq article-list (gen-gnd))

;; Put the main program in a node.

(ins-nod (get-gnd)
	 '((list (foo 5)
		 (goo 6)))
	 (get-gnd))

=> 1

;; Put two subroutines in their own nodes.

(ins-nod (get-gnd)
	 '((* x x))
	 (get-gnd))

=> 2

(ins-nod (get-gnd)
	 '((+ x 3))
	 (get-gnd))
=> 3

;; Make links from the main program to its subroutines.  The text of
;; the link states the name of the subroutine.

(ins-nod 1 '(sub foo) 2)
=> 4

(ins-nod 1 '(sub goo) 3)
=> 5

;; Add nodes and links for the variables.

(ins-nod (get-gnd)
	 nil
	 (get-gnd))
=> 6

;; Argument list of node 1 (points to node 6, which contains nil)
;; In order words, List 1 doesn't have any unbound variables.
(ins-nod 6 'var 1)
=> 7

(ins-nod (get-gnd)
	 '(x)
	 (get-gnd))
=> 8

;; Argument listS of node 2 and node 3
(ins-nod 8 'var 2)
=> 9

(ins-nod 8 'var 3)
=> 10

; We provide functions to identify scholia of type
;; var and sub and retrieve the appropriate data.

(defun get-dependencies (art)
  (mapcar '(lambda (x)
	     (list (cadr (get-txt x))
		   (get-snk x)))
	  (filter
	   '(lambda (y)
	      (equal 'sub 
		     (car (get-txt y))))
	   (get-flk art))))

(defun get-vars (art)
  (delete-dups
   (apply 'append
	  (mapcar 
	   '(lambda (x)
	      (get-txt (get-src x)))
	   (filter
	    '(lambda (y)
	       (equal 'var (get-txt y)))
	    (get-blk art))))))

;; Here is the output they produce:

(get-dependencies 1)
  =>
((goo 3) (foo 2))

(get-vars 2)
  =>
(x)

;; Using these functions, we evaluate our node.  Remember that the code
;; at node 1 is supposed to invoke foo and goo, which are found as
;; scholia attached to node 2.  As we see, it does this correctly.

(funcall (node-fun 1
	  'get-txt
	  'get-vars 
	  'get-dependencies))
=> (25 9)

;; In case you're interested, here are the gory details of how
;; node-fun wrapped up the code inside the node.

(node-fun 1 
	  'get-txt
	  'get-vars 
	  'get-dependencies)
  => 
(lambda nil (flet ((goo (x) (apply (node-fun 3
					     (quote get-txt)
					     (quote get-vars)
					     (quote get-dependencies))
				   (list x)))
		   (foo (x) (apply (node-fun 2
					     (quote get-txt)
					     (quote get-vars)
					     (quote get-dependencies))
				   (list x))))
	      (list (foo 5) (goo 6))))


(node-fun 3
	  'get-txt
	  'get-vars 
	  'get-dependencies)
=>
(lambda (x) (flet nil (+ x 3)))

(get-txt 3) => ((+ x 3))
(get-vars 3) => (x)
(get-dependencies 3) => nil


(get-txt 6) => nil
(set-txt 6 '(y)) => (y)

(node-fun 1
	  'get-txt
	  'get-vars 
	  'get-dependencies)
=> (lambda (y) (flet ((goo (x) (apply (node-fun 3 (quote get-txt) (quote get-vars) (quote get-dependencies)) (list x))) 
		      (foo (x) (apply (node-fun 2 (quote get-txt) (quote get-vars) (quote get-dependencies)) (list x)))) 
		 (list (foo 5) (goo 6))))

(get-txt 1) => ((list (foo 5) (goo 6)))
(set-txt 1 '((list (foo 5) (goo  y))))((list (foo 5) (goo y)))

(node-fun 1
	  'get-txt
	  'get-vars 
	  'get-dependencies)
(defun joe-function (y)
  (flet ((goo (x) (apply (node-fun 3 (quote get-txt) (quote get-vars) (quote get-dependencies)) (list x)))
	 (foo (x) (apply (node-fun 2 (quote get-txt) (quote get-vars) (quote get-dependencies)) (list x))))
    (list (foo 5) (goo y))))

(joe-function 16)(25 19)

(funcall (node-fun 1
		   'get-txt
		   'get-vars 
		   'get-dependencies)
	 16)(25 19)
