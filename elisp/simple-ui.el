
(defvar simple-ui-buff (list nil (make-list 7 nil) nil))
;; This variable stores three items: the buffer in which
;; the nema is to be edited, markers inside that buffer
;; which delimit the text which the user supplies, and
;; the number of the nema being edited.

(defun simple-ui-init ()
  "Initialize the buffer for editing nemata."
  (interactive)
  (let ((buff (get-buffer-create "nema-edit"))
	(marx nil)
	(places nil))
    (cl-flet ((add-rubric (pre mid post)
			  (insert
			   (concat
			    (propertize pre
					'face 'bold
					'read-only "Rubric."
					'front-sticky nil
					'rear-nonsticky nil)
			    (propertize mid
			       'face 'bold
			       'read-only "Rubric."
			       'front-sticky t
			       'rear-nonsticky nil)
			    (propertize post
					'face 'bold
					'read-only "Rubric."
					'front-sticky t
			       'rear-nonsticky t)))))
	      ;; Since the buffer may contain read-only
	      ;; text, we must inhibit the read-only
	      ;; property in order to erase such text.
	      (with-current-buffer buff
		(let ((inhibit-read-only t))
		  (erase-buffer))
		(dotimes (n 7)
		  (push (make-marker) marx))
		(add-rubric "" "Editing nema" ":")
		(push (- (point) 1) places)
		(push (+ (point) 1) places)
		(add-rubric "\n" "Source" ":")
		(push (- (point) 1) places)
		(insert " ") 
		(push (+ (point) 1) places)
		(add-rubric "\n" "Sink" ":")
		(push (- (point) 1) places)
		(insert " ")
		(push (+ (point) 1) places)
		(add-rubric "\n" "Content" ":")
		(push (- (point) 1) places)
		(insert " ")
		(mapply 'set-marker `(,marx ,(reverse places)))		
		(setq simple-ui-buff (list buff marx nil)))))
	      simple-ui-buff)

(defun simple-ui-load (nema)
  "Load a nema into the editing buffer."
  (interactive "xNema uid:")
  (if (uid-p nema)
      (cl-flet (
		;; Helper function to figure out character
		;; positions between which text goes.
		(spot (n)
		      (if (< n 7)
		       (+ (marker-position
			   (nth n (nth 1 simple-ui-buff)))
			  (- 1 (* (% n 2) 2)))
		     (point-max))))
      (with-current-buffer (nth 0 simple-ui-buff)
	;; Replace nema
	(delete-region (spot 0) (spot 1))
	(goto-char (spot 0))
	(insert (prin1-to-string nema))
	;; Replace source.
	(delete-region (spot 2) (spot 3))
	(goto-char (spot 2))
	(insert (prin1-to-string (get-source nema)))
	;; Replce sink.
	(delete-region (spot 4) (spot 5))
	(goto-char (spot 4))
	(insert (prin1-to-string (get-sink nema)))
	;; Replace content
	(delete-region (spot 6) (spot 7))
	(goto-char (spot 6))
	(insert (prin1-to-string (get-content nema)))
	(setcar (cddr simple-ui-buff) nema))
      nema)
    'huh))

(defun simple-ui-new ()
  "Make a new nema and edit it."
  (interactive)
  (simple-ui-load
   (add-nema 0 0 nil)))


(defun simple-ui-save ()
  "Save the nema in the editing buffer."
  (interactive)
  (if (uid-p (nth 2 simple-ui-buff))
      (cl-flet (
		;; Helper function to figure out character
		;; positions between which text goes.
		(spot (n)
		      (if (< n 7)
			  (+ (marker-position
			      (nth n (nth 1 simple-ui-buff)))
			     (- 1 (* (% n 2) 2)))
			(point-max))))
	(with-current-buffer (nth 0 simple-ui-buff)
	  (update-source
	   (nth 2 simple-ui-buff)
	   (car (read-from-string
		 (buffer-substring (spot 2) (spot 3)))))
	  (update-sink
	   (nth 2 simple-ui-buff)
	   (car (read-from-string
		 (buffer-substring (spot 4) (spot 5)))))
	  (update-content
	   (nth 2 simple-ui-buff)
	   (car (read-from-string
		 (buffer-substring (spot 6) (spot 7))))))
	(nth 2 simple-ui-buff))
    nil))
