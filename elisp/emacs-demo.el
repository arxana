;; Read in the files.

(push "/home/rsp/Documents" load-path)
(require 'cl)
(load-file "prelim.el")
(load-file "honey-redux.el")
(load-file "simple-ui.el")

(global-set-key (kbd "C-c i") 'simple-ui-init)
(global-set-key (kbd "C-c l") 'simple-ui-load)
(global-set-key (kbd "C-c n") 'simple-ui-new)
(global-set-key (kbd "C-c s") 'simple-ui-save)

(set-current-plexus (add-plexus))

(add-nema 0 0 "walrus")
(add-nema 0 0 "mammal")
(add-nema 2 3 nil)
(label-nema 2 "walrus")
(label-nema 3 "mammal")
(label2uid "mammal")
(uid2label 2)
(get-content 2)
(get-source 4)
(get-sink 4)
(uid2label (get-sink 4))
(get-content (label2uid "walrus"))
   
(download-en-masse)

