;; bootstart.el - Basic functions for literate scholium-based programming

;; Copyright (C) 2012, 2013 Raymond S. Puzio

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; COMMENTARY:

;;; CODE:

(require 'cl)

(defun tangle-module (node retrieve-cont retrieve-links)
  (insert-chunk
   (funcall retrieve-cont node)
   (funcall retrieve-links node)
   retrieve-cont 
   retrieve-links))

(defun insert-chunk (body chunks retrieve-cont retrieve-links)
  (cond ((null body) nil)
	((null chunks) body)
	((assoc body chunks)
	 (tangle-module
	  (cadr (assoc body chunks))
	  retrieve-cont
	  retrieve-links))
	((atom body) body)
	(t (cons (insert-chunk (car body) chunks retrieve-cont retrieve-links)
		 (insert-chunk (cdr body) chunks retrieve-cont retrieve-links)))))

(defun node-fun (node retrieve-code 
		      retrieve-vars
		      retrieve-link)
  (list (quote lambda)
	(funcall retrieve-vars node)
	(cons (quote flet)
	      (cons (mapcar (quote (lambda (item)
				     (let ((var-list
					    (funcall retrieve-vars (cadr item))))
				       (\` ((\, (car item))
					    (\, var-list)
					    (apply (node-fun (\, (cadr item)) 
							     (quote (\, retrieve-code))
							     (quote (\, retrieve-vars))
							     (quote (\, retrieve-link)))
						   (\, (cons (quote list) var-list)))))))) 
			    (funcall retrieve-link node))
		    (funcall retrieve-code node)))))

(defun get-src (n) 
  (car (nth 0 (cdr (assoc n (cdr article-list))))))

(defun get-flk (n)
  (cdr (nth 0 (cdr (assoc n (cdr article-list))))))

(defun get-txt (n)
  (nth 1 (cdr (assoc n (cdr article-list)))))

(defun get-snk (n)
  (car (nth 2 (cdr (assoc n (cdr article-list))))))

(defun get-blk (n)
  (cdr (nth 2 (cdr (assoc n (cdr article-list))))))

(defun get-ids nil 
  (mapcar (quote car) (cdr article-list)))

(defun get-gnd nil 0)

(defun set-src (n x)
  (if (equal n 0) 
      0 
    (progn (let ((old-backlink 
		  (nth 1 (assoc (get-src n) (cdr article-list)))))
	     (setcdr old-backlink (delete n (cdr old-backlink))))
	   (let ((new-backlink 
		  `(nth 1 (assoc x (cdr article-list)))))
	     (setcdr new-backlink (cons n (cdr new-backlink))))
	   (setcar (nth 1 (assoc n (cdr article-list))) x))))

(defun set-txt (n x)
  (setcar (cdr (cdr (assoc n (cdr article-list)))) x))

(defun set-snk (n x)
  (if (equal n 0) 
      0 
    (progn (let ((old-backlink
		  (nth 3 (assoc (get-snk n) (cdr article-list)))))
	     (setcdr old-backlink (delete n (cdr old-backlink)))) 
	   (let ((new-backlink
		  (nth 3 (assoc x (cdr article-list)))))
	     (setcdr new-backlink (cons n (cdr new-backlink)))) 
	   (setcar (nth 3 (assoc n (cdr article-list))) x))))

(defun ins-nod (src txt snk)
  (progn (setcdr article-list
		 (cons (list (car article-list)
			     (list src)
			     txt
			     (list snk))
		       (cdr article-list)))
	 (let ((backlink
		(nth 3 (assoc snk (cdr article-list)))))
	   (setcdr backlink (cons (car article-list) (cdr backlink))))
	 (let ((backlink
		(nth 1 (assoc src (cdr article-list)))))
	   (setcdr backlink (cons (car article-list) (cdr backlink))))
	 (- (setcar article-list (+ 1 (car article-list))) 1)))

(defun del-nod (n)
  (if (or (equal n 0)
	  (get-blk n)
	  (get-flk n))
      nil
    (progn (let ((old-backlink
		  (nth 3 (assoc (get-snk n) (cdr article-list)))))
	     (setcdr old-backlink (delete n (cdr old-backlink))))
	   (let ((old-backlink
		  (nth 1 (assoc (get-src n) (cdr article-list)))))
	     (setcdr old-backlink (delete n (cdr old-backlink))))
	   (setcdr article-list 
		   (delete (assoc n (cdr article-list))
			   (cdr article-list))) 
	    t)))
