(defun node-fun (node get-code get-links)
  (let ((code  (funcall get-code node))
	(links (funcall get-links node)))
    (list
     'lambda 
     (car code)
     (cons 
      'prog1
      (cons
       (append
	'(progn)
	; Produce a list of commands to produce temporary bindings.
	(mapcar '#(lambda (x)
		    `(fset ',(car x)
			   (node-fun ,(cdr x) 
				     ',get-code 
				     ',get-links)))
		links)
	(cdr code))
       ;Produce a list of commands to reset function values.
       (mapcar '#(lambda (x)
		   (if (fboundp (car x))
		       `(fset ',(car x)
			      ',(symbol-function (car x)))
		     `(fmakunbound ',(car x))))
	       links))))))

;; Recursively replace the chunks to recover executable code.

(defun tangle-module (node get-cont ins-links)
  (insert-chunk 
   (funcall get-cont node)
   (mapcar '#(lambda (x)
	       (cons (car x)
		     (tangle-module (cdr x)
				    get-cont
				    ins-links)))
	   (funcall ins-links node))))

;; Given a node and an association list of replacement texts, insert
;; the chunks at the appropriate places.

(defun insert-chunk (body chunks)
  (cond ((null body) nil)
	((null chunks) body)
	((equal (car body) '*insert*)
	 (cdr (assoc (cadr body) chunks)))
	(t (cons (insert-chunk (car body) chunks)
		 (insert-chunk (cdr body) chunks)))))
