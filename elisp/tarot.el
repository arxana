;;; tarot.el -- draw random tarot cards and compile html web pages

;; Copyright (C) 2007, 2011, 2013 Joseph Corneli <holtzermann17@gmail.com>

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Alt text by Crowley and/or Alliette where applicable.

;; M-x tarot-draw-card    - to draw one card.
;; M-x tarot-spread       - to draw four cards and possibly get some associations.
;; M-x tarot-arxana-print - to print out all the available associations in one buffer.

;;; Code:

(defvar tarot-deck [["Fool"               "La Follie ou l'Alchemiste"]
                    ["Magician"           "Le Magicien ou le Bateleur"]
                    ["High Priestess"     "Repos"]
                    ["Empress"            "Les Astres"]
                    ["Emperor"            "Les Ouiseaux et les Poissons"]
                    ["Hierophant"         "Le Grand pretre"]
                    ["Lovers"             "Le Chaos"]
                    ["Chariot"            "Le Despote africain"]
                    ["Strength"           "La Force"]
                    ["Hermit"             "Le Capucin"]
                    ["Wheel of Fortune"   "La Roue de Fortune"]
                    ["Justice"            "La Justice"]
                    ["Hanged Man"         "La Prudence"]
                    ["Death"              "La Mort"]
                    ["Temperance"         "La Temperance"]
                    ["Devil"              "Le Diable"]
                    ["Tower"              "Le Temple Foudroye"]
                    ["Star"               "La Ciel"]
                    ["Moon"               "Les Plantes"]
                    ["Sun"                "La Lumiere"]
                    ["Judgement"          "Le Jugement Dernier"]
                    ["World"              "L'homme et les Quadrupedes"]
                    ["Ace of Wands"       "The Root of the Powers of Fire"]
                    ["2 of Wands"         "Dominion"]
                    ["3 of Wands"         "Virtue"]
                    ["4 of Wands"         "Completion"]
                    ["5 of Wands"         "Strife"]
                    ["6 of Wands"         "Victory"]
                    ["7 of Wands"         "Valour"]
                    ["8 of Wands"         "Swiftness"]
                    ["9 of Wands"         "Strength"]
                    ["10 of Wands"        "Oppression"]
                    ["Page of Wands"]
                    ["Knight of Wands"]
                    ["Queen of Wands"]
                    ["King of Wands"]
                    ["Ace of Cups"        "The Root of the Powers of Water"]
                    ["2 of Cups"          "Love"]
                    ["3 of Cups"          "Abundance"]
                    ["4 of Cups"          "Luxury"]
                    ["5 of Cups"          "Disappointment"]
                    ["6 of Cups"          "Pleasure"]
                    ["7 of Cups"          "Debauch"]
                    ["8 of Cups"          "Indolence"]
                    ["9 of Cups"          "Happiness"]
                    ["10 of Cups"         "Satiety"]
                    ["Page of Cups"]
                    ["Knight of Cups"]
                    ["Queen of Cups"]
                    ["King of Cups"]
                    ["Ace of Swords"      "The Root of the Powers of Air"]
                    ["2 of Swords"        "Peace"]
                    ["3 of Swords"        "Sorrow"]
                    ["4 of Swords"        "Truce"]
                    ["5 of Swords"        "Defeat"]
                    ["6 of Swords"        "Science"]
                    ["7 of Swords"        "Futility"]
                    ["8 of Swords"        "Interference"]
                    ["9 of Swords"        "Cruelty"]
                    ["10 of Swords"       "Ruin"]
                    ["Page of Swords"]
                    ["Knight of Swords"]
                    ["Queen of Swords"]
                    ["King of Swords"]
                    ["Ace of Pentacles"  "The Root of the Powers of Earth"]
                    ["2 of Pentacles"    "Change"]
                    ["3 of Pentacles"    "Work"]
                    ["4 of Pentacles"    "Power"]
                    ["5 of Pentacles"    "Worry"]
                    ["6 of Pentacles"    "Success"]
                    ["7 of Pentacles"    "Failure"]
                    ["8 of Pentacles"    "Prudence"]
                    ["9 of Pentacles"    "Gain"]
                    ["10 of Pentacles"   "Wealth"]
                    ["Page of Pentacles"]
                    ["Knight of Pentacles"]
                    ["Queen of Pentacles"]
                    ["King of Pentacles"]])

(defun random-aref (array)
  (let ((index (random (length array))))
    (values (aref array index) index)))

(defun read-tarot-card (card)
  (let ((len (length card)))
      (if (= len 2)
          (concat (aref card 0) " [" (aref card 1) "]")
        (aref card 0))))

(defun tarot-card ()
  (multiple-value-bind
      (card index) (random-aref tarot-deck)
    (read-tarot-card card)))

(defun tarot-draw-card ()
  (interactive)
  (message "%s" (tarot-card)))

(defun tarot-cards (n)
  (let ((pack tarot-deck)
        stack)
    (dotimes (i n)
        (multiple-value-bind
            (card index) (random-aref pack)
          (setq stack (cons card stack))
          (setq pack (delete (aref pack index) pack))))
    stack))

(defun tarot-spread ()
  (interactive)
  (pop-to-buffer (get-buffer-create "*Spread*"))
  (erase-buffer)
  (let ((stack (tarot-cards 4))
        (import '("Past    :"
                  "Present :"
                  "Future  :"
                  "Outcome :")))
    ;; print out the spread itself
    (dolist (card stack)
      (insert (car import) " ")
      (setq import (cdr import))
      (insert (read-tarot-card card) "\n"))
    ;; add associations
    (dolist (card stack)
      (let ((possible-association (cdr (assoc (aref card 0) tarot-arxana-associations))))
      (if possible-association
          (insert "\n" possible-association "\n"))))
    (insert "\n"))
  (goto-char (point-min)))

(defun add-html-paragraphs (txt)
  (replace-regexp-in-string "\\(.\\)$" "\\1</p>" (replace-regexp-in-string "^\\(.\\)" "<p>\\1" txt t) t))

(defun tarot-arxana-print ()
  (interactive)
  (pop-to-buffer (get-buffer-create "*Arxana*"))
  (erase-buffer)
  ;; header and title
  (insert "<!DOCTYPE html>
<html><head>
<meta charset=\"utf-8\" />
<title>Arxana.net</title>
<style>
p {text-align:justify;}
table {border-spacing: 50px 10px;}
a:link { color: #000000; text-decoration: underline;}
a:visited { color: #000000; text-decoration: underline;}
a:active { color: #000000; text-decoration: underline;}
a:hover { color: #000000; text-decoration: underline;}
img { border:1px solid #FFFFFF; -webkit-border-radius: 4px; -moz-border-radius: 4px; border-radius: 4px;}
</style>
</head>
<body><h1>All Yesterday's Tomorrows: The report on, and
of, Project Arxana concerning word processing, electronic publishing,
hypertext, etc.</h1>
<p style=\"padding:10px;\">Arxana is a free/open hypertext system written in Emacs Lisp, with Common Lisp extensions.</p>
<p style=\"padding:10px;\">By Joseph Corneli and Raymond S. Puzio</p>")
  ;; document body
  (let ((count 0))
    (mapc (lambda (a)
            (insert
             (let* ((filename (replace-regexp-in-string " " "-"
                                                        (car a)))
                    (content (cdr a))
                    ;; format the associated image
                    (image (concat "<img alt=\"" (upcase filename) "\" src=\"http://metameso.org/~joe/" (upcase filename) ".png" "\">"))
                    ;; format the associated text
                    (text (add-html-paragraphs content)))
                    (concat
                     "<table>\n<tr>\n<td>"
                     ;; alternate image left and right
                     (if (evenp count) image text)
                     "</td><td>\n\n"
                     ;; alternate image left and right
                     (if (evenp count) text image)
                     "</td>\n</tr>\n</table>"
                     )))
             (setq count (1+ count)))
            tarot-arxana-associations))
    ;; footer
    (insert "<br />
<table>
<tr>
<td style=\"width:50% !important;\">
Arxana's <a href=\"http://repo.or.cz/w/arxana.git\">source code</a> is available under the terms of the <a href=\"http://www.gnu.org/licenses/agpl-3.0.html\">Affero GNU GPL 3.0</a>. &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</p>
<td style=\"width:50% !important;text-align:right;\">
<a href=\"http://cliki.net\"><img alt=\"Made with LISP\" src=\"./lisplogo.png\"></a>
<span style=\"position:relative;bottom:-2\"><a href=\"http://www.gnu.org/software/emacs/\"><img alt=\"Made with GNU Emacs\" src=\"./gnuheadgubinelli.png\"></a></span>
</td>
</tr></table>
</body>")
    (goto-char (point-min)))

; (defvar tarot-arxana-associations
(setq tarot-arxana-associations '(("Fool" .
                                   "The idea is to refactor pieces of information into documents in a “holographic” fashion. This addresses the idea that a document is made up of paragraphs, a paragraph is made up of sentences, sentences are made up of words, and words are made up of letters -- but also that the world is not as simple and hierarchical as this picture might make you think.

Newspapers are made up of articles, but they can be folded into hats or taped into Möbius strips.  Math papers are made up of definitions, theorems, and proofs, which can turn out to be incorrect. Comic books are made up of panels, art, and speech balloons, but they can also be made into movies.

A tarot spread is made up of cards that are arranged and narrated in a way that stimulates the mind.")
                                  ("Magician" .
                                   "Over the years, we've built several prototypes and spin-offs, ranging from <a href=\"http://permalink.gmane.org/gmane.emacs.sources/507\">a simple ipod-like list browser</a>, to a <a href=\"http://planetmath.org/node/42412\">representation language</a> for mathematics that envisioned definitions and theorems as lists of assertions and instantiations.

Our aim for the project is to build something that's useful for authoring complicated documents with lots of interconnected and interrelated bits.  <a href=\"http://www.gnu.org/software/emacs/\">Emacs</a> was the natural starting place.

We presented the first really usable prototype of the system at a  <i>Symposium on Free Culture and the Digital Library</i> in 2005, along with a paper that proposed a <a href=\"http://metameso.org/~joe/docs/sbdm.html\">Scholium-Based Document Model for Commons Based Peer Production</a>.")
                                  ("High Priestess" .
                                   "Currently the defining features of an Arxana implementation are: (1) low-level functions that allow you to create a network of texts; (2) a browser to navigate the graph; (3) functions that will assemble documents out of the graph.

In particular, we've created a programming framework for writing programs as graphs. Programs written in this manner can either be run in situ by a graphical interpreter or compiled down into traditional code for running outside the system.
This programming facility allows for literate programming in which programs are built out of chunks by transclusion and comments are attached as scholia. The same ideas applies to text: they can either be browsed as hypertext, or specific portions can be compiled into printable documents on the fly.")
                                  ("Empress" .
                                   "Accessing bits and pieces in different ways should be possible with multiple backends (e.g. database, web, memory).

Hypertext nouveau is based on the concept of semantic triples of the form subject, predicate, object.  This rather constrained language allows one to say quite a lot.  The picture that emerges is something like the constellations that we superimpose over the stars.    We've had lots of interesting conversations about how to represent things, will we need 29 place terms?  probably not...

The previous 2009-era prototype used CLSQL to interface to MySQL and represented links as triples in a handmade triple (or rather, quad) store.  We also experimented with using cl-elephant as a storage system to persist nodes as objects.")
                                  ("Emperor" .
                                   "Frontend features rely on Emacs text properties, and support the ability to edit multiple nodes at one time, and have the changes routed back into the backend properly.

Previous Emacs-based browsing systems include Help, Info, and Emacs/w3m.  Editing and browsing with these systems were essentially completely distinct activities.  Although completely silly, M-x doctor provides encouragement that an interactive system could do something interesting.  

Various experiments with the social web point in the direction of an open read/write platform, but typically these experiments have had very limited semantics.  We're thinking we can do more here.")
                                  ("Hierophant" .
                                   "We're planning middle-end features that will be associated not just with assembling texts on the fly, but doing additional processing, like proof checking.

One reason for choosing our data model is that inference rules can be represented very naturally as networks.  The linear representations that we're used to is only a representation of the way people actually think about things.  At some point in the future, we hope to be able to turn Arxana networks into schematic diagrams automatically!

For now, here's a <a href=\"http://arxana.net/inference-example.jpg\">hand-drawn picture</a> showing how inference rules look when they're put presented in a network structure.")
                                  ("Lovers" .
                                   "What can we do to interact with objects once they've been properly parsed?  There will be some interesting experiments in the future, connecting Arxana and PlanetMath, which will be particularly nice now that the latter has been re-built using <a href=\"http://dlmf.nist.gov/LaTeXML/\">LaTeXML</a>.  

All of the math on PlanetMath is now rendered in Content MathML, which should give us plenty of things to chew on (and in the not-too-distant future, we'll have <a href=\"kwarc.info/kohlhase/submit/iSemantics10.pdf\">sTeX and OMDoc</a> to chew on as well).

Whether on PlanetMath or beyond, Emacs and Drupal might be a match made in heaven!  One of the most interesting applications we have in mind that should be available in coming days is a connection between Arxana and Drupal, using Drupal's <a href=\"http://drupal.org/project/services\">Services</a> module."
                                   )
                                  ("Chariot" .
                                   "As interesting as the mathematics applications are, the proper vehicle for this system is literate programming, or even better, <i>literary</i> programming in Lisp.  <a href=\"http://www.gigamonkeys.com/book/introduction-why-lisp.html\">Why Lisp?</a> is a question that has been asked and answered many times.

Donald Loritz <a href=\"https://calico.org/a-353-An%20Introductory%20Lisp%20Parser.html\">writes</a>: “in Lisp, a Saussurean arbitrary relation holds between signifier and significand [...] it is possible to redefine virtually every classical Lisp command word.” 

Our point of view on network programming is similar, but it goes beyond Lisp in several important ways.  First, our links are bidirectional, and second, we allow objects to be attached at any point in the network, not just at the tree boundaries.  In Lisp, what you have are tuplets, not triplets.  If you want to link to content, you have to put it in a CAR or a CDR, which gives you one link left, which is enough to create chains, but not any more interesting graph structures."
                                   )
                                  ("Strength" .
                                   "In sum, pretty much every object in this system is annotatable.  This is Arxana's biggest strength, which, as is well known, often represents the greatest weakness.  In the first place, making this statement precise has been tricky.

Secondly, if you make everything annotatable, you're bound to get a representation that's more complicated than what you might get if you wanted to just compute.

The system can be flexible, but it can also be formal; for example, if we wish, we can implement a type system in links."
                                   )
                                  ("Hermit" .
                                   "How do we intend to connect with other developers?  We already have connections to KWARC, and have presented work in progress several times at LISP NYC.

One of the most relevant places to look for connections is the Free Technology Guild, particularly as we think about ways to implement Richard P. Gabriel and Ron Goldman's <a href=\"http://www.dreamsongs.com/MobSoftware.html\">Mob Software</a> ideas.

One of the most straightforward ways to explain this idea is that we want to build a wiki made of programs and documentation instead of text."
                                   )
                                  ("Wheel of Fortune" .
                                   "At present, real-time interactions like Etherpad and ShareJS are popular.  It would be great to be able to integrate these things into the system.  Ted Nelson is also interested in multimedia -- why not annotate video using this system? 

If Douglas Adams were still alive, maybe he would be able to use a system like this one to sell his next novel, using a Namecoin distribution method.

If someone is listening to a lecture and taking notes, it would be great to live stream the notes that people are taking along with the video -- and save them so that the video can be read along with the notes later.")
                                  ("Justice" .
                                   "Linking is how scholia are attached to other articles; the collection of all articles is ``the commons''; and the rules for interacting with this collection defines the commons's regulatory system.

The idea of using Arxana to model the commons has some staggering implications.   A network is just a way to take a shared system of some form and make it computational.  Once you have a good model, you want to be able to interact with the system in some way.

If you want to organize for social change, you need a system of annotations that's open and robust."
                                   )
                                  ("Hanged Man" .
                                   "We will connect Emacs to Common Lisp via <a href=\"http://common-lisp.net/project/slime/\">Slime</a>, and Common Lisp to PostgreSQL via <a href=\"http://clsql.b9.com/\">CLSQL</a>.  CLSQL also talks directly to the <a href=\"http://www.sphinxsearch.com/\">Sphinx search engine</a>, which we use for text-based search. Once all of these things are installed and working together, you should be able to begin to use Arxana.

This prototyping work didn't go particularly well, and ended up having several regressions with respect to the previous version of the code.   But at the same time, we've been able to re-use code or from previous prototypes.  

Now that we're making a cleaner separation of backend, middle-end, and front end, we'll have easier ways to swap things in and out.  There will be less concern about which choice to make, because we can use any implementation for a module.  This is a design principle that goes fairly deep into the system, so, anything that implements a 'pow' function can be used in that way.")
                                  ("Death" .
                                   "The reader will perhaps have noticed the similarities and references to the work and ideas of Ted Nelson.  We want to be clear that the system presented here isn't an implementation of the Xanadu™ idea, per se, although it provides some of the features one would expect from a “Xanadu™ implementation.”

One clear difference between this system and the Xanadu™ system is that here articles are not supposed to be presented in a pay-to-access fashion.  The Xanadu™ system was intended to be Free as in Freedom (in a certain limited sense), but not Free as in Beer.  We might recover some micropayment features eventually, but it's not a core focus.

In any case, the main concern with Xanadu™ is that it's <a href=\"http://www.wired.com/wired/archive/3.06/xanadu.html\">cursed</a>.  We didn't want the same fate to befall Arxana, which was why we took a 100% free/open source approach.  That said, we probably didn't have the best strategy for outreach and connection with other developers as we were getting started!")
                                  ("Temperance" .
                                   "We would have also done well to pay more attention to the philosophical foundations of hypertext from the very start.  Wittgenstein remarked ironically: “It should be possible to decide a priori whether, for example, I can get into a situation in which I need to symbolize with a sign of a 27-termed relation.” (Tractatus 5.5541)

Another interesting connection from philosophy is Theuth (AKA Thoth), as understood by Derrida, and the associated notions of deconstruction and the <a href=\"http://faculty.arts.ubc.ca/pmahon/pharmakon.html\">pharmakon</a>.

One of the key points to keep in mind are the limits of any system -- we could quote also from Borges or Deleuze, but here's Wittgenstein again: “And how would it be possible that I should have to deal with forms in logic which I can invent: but I must have to deal with that which makes it possible for me to invent them.” (Tractatus 5.555)")
                                  ("Devil" . 
                                   "How do we deal with content that isn't authored in Arxana?  -- Especially if it is locked up behind non-free protocols.

In some sense there are circles of Hell here -- something you can't get any reading on whatsoever is a brick, but you can still attach comments to just about anything, even a brick.

Other things you can model.")
                                  ("Tower" .
                                   "Looking back over the history of the project, another key point of reference is Dirk Gently's Holistic Detective Agency.  Perhaps you remember that the pseudo-protagonist, Richard MacDuff, got into programming when he was working on creating a text editor to edit his English papers with.  

Hypertext can often feel like a distraction from a distraction from a distraction.  (Dirk Gently author Douglas Adams was famous for his procrastination, and would generally only finish novels when his agent locked him in a hotel room.)

Anyway, the literary, textual, and philosophical references go <a href=\"http://wiki.planetmath.org/cgi-bin/wiki.pl/historical_scholia_systems\">way back</a>, for instance to the Talmud and its history.  For a more contemporary example, Bukowski's books happened to show up in the Xanadu bookstore in Memphis, where one of us was buying cheap geometry differential texts in bulk.")
                                  ("Star" .
                                   "Assembling a document from various disparate sources in the database is reminiscent of the way p2p filesharing works.

There are some interesting developments with Bitcoin and more recently with Namecoin that we could potentially connect up with here.

Apart from Namecoin, an early aim of the system was to use distributed databases to make an alternative hypertext system much more in line with Ted Nelson's ideas.  Well, perhaps one day.")
                                  ("Moon" .
                                   "There was probably a non-trivial chance of going crazy when working on this system.  We create a lot of obscure documents on <a href=\"http://wiki.planetmath.org/\">AsteroidMeta</a> for example.

The question with this system was always a practical one.  So far, it hasn't been all that practical, but all in good time?  

“Marry, then, sweet wag, when thou art king, let not us that are squires of the night's body be called thieves of the day's beauty: let us be Diana's foresters, gentlemen of the shade, minions of the moon; and let men say we be men of good government, being governed, as the sea is, by our noble and chaste mistress the moon, under whose countenance we steal.”")
                                  ("Sun" .
                                   "If you put together all of the different things that we have here, you might get something like the <a href=\"http://wiki.planetmath.org/cgi-bin/wiki.pl/The_Hyperreal_Dictionary_of_Mathematics\">Hyperreal Dictionary of Mathematics</a> that we've talked about for years.   If we get the real-time co-editing system set up, with some nice co-presence markers and use this system to build a real-time editable math MUD, that would be pretty practical even without the Artificial Intelligence part.

Networks and nodes are a really interesting thing to work with here.  Our networks are more lively than the typical subject, predicate, object -- we're much more in the mind to represent beginning, middle, and end, representing dynamic process as opposed to static things.

What quotation does is it represents as a thing some process from a lower level theory.  If A is a meta-theory to B, then you can start to reason about things at a higher level.  The term ``format shifting'' belies the nature and power of simulation, and the true force of medium-as-message.")
                                  ("Judgement" .
                                   "Ted Nelson's “Literary Machines” and Marvin Minsky's “Society of Mind” are important inspirations.  Alfred Korzybski's “Science and Sanity” and Gilles Deleuze's “The Logic of Sense” provided some grounding and encouragement early on.  LaTeX and GNU Emacs have been useful not just in prototyping this system, but also as exemplary projects in the genre.  John McCarthy's <a href=\"http://www-formal.stanford.edu/jmc/elephant/elephant.html\">Elephant 2000</a> was an inspiring thing to look at and think about, and of course Lisp has been a vital ingredient.

More recently, the conceptual artwork <a href=\"http://nickm.com/post/2012/08/a-thousand-twitters/\">Monolyth</a> gave some indication of the form we wanted this document to take -- 14000 characters, a kilotweet, which we will continue to revise as the project develops.

On the formal side, our approach has been informed by various concepts from the foundations of mathematics such as Quine's distinction of use versus mention, Tarski's consequence operator approach to logical theories, Lesniewski's mereology, Makarov's D-logic, Grothendieck's categorical geometry and Lawvere's categorical logic.  One of our objectives is to bring some of these ideas down to earth from their lofty perch atop the tower of mathematical abstraction and embody them in computer code which can be applied to practical problems.")
                                  ("World" .
                                   "In short, Hello World!  This document is readable and editable within Arxana, as is the system itself.

We're interested to get other people involved as easily as possible -- for now we've got code on git.or.cz, but that will be followed soon with a Github account, and links to our latest download.

There's even an <a href=\"https://groups.google.com/group/arxana-talk\">arxana-talk mailing list</a>, if you want to get in touch with us directly.")))

;;; tarot.el ends here
