;; sch-prog-demo.el - Minimal example of scholiumific programming

;; Copyright (C) 2013 Raymond S. Puzio

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; COMMENTARY:

;; See http://arxana.net/foo-goo.jpg for a vital map.

;;; CODE:

(load-file "prelim.el")
(load-file "honey-redux.el")
(require 'cl)

(defun node-fun (node retrieve-code 
		      retrieve-vars
		      retrieve-link)
  (list (quote lambda)
	(funcall retrieve-vars node)
	(cons (quote flet)
	      (cons (mapcar (quote (lambda (item)
				     (let ((var-list
					    (funcall retrieve-vars (cadr item))))
				       (\` ((\, (car item))
					    (\, var-list)
					    (apply (node-fun (\, (cadr item)) 
							     (quote (\, retrieve-code))
							     (quote (\, retrieve-vars))
							     (quote (\, retrieve-link)))
						   (\, (cons (quote list) var-list)))))))) 
			    (funcall retrieve-link node))
		    (funcall retrieve-code node)))))

;; Make a new scholium-based document.

(set-current-plexus (add-plexus))

;; Put the main program in a node.

(label-nema
 (add-nema (label2uid "ground")
	   (label2uid "ground")
	   '((list (foo 5)
		   (goo 6))))
 "main-prog")

;; Put two subroutines in their own nodes.

(label-nema
 (add-nema (label2uid "ground")
	   (label2uid "ground")
	   '((* x x)))
 "sub-foo")

(label-nema
 (add-nema (label2uid "ground")
	   (label2uid "ground")
	   '((+ x 3)))
 "sub-goo")

;; Make links from the main program to its subroutines.  The text of
;; the link states the name of the subroutine.

(label-nema
 (add-nema
  (label2uid "main-prog")
  (label2uid "sub-foo")
  '(sub foo))
 "link-foo")

(label-nema
 (add-nema
  (label2uid "main-prog")
  (label2uid "sub-goo")
  '(sub goo))
 "link-goo")

;; Add nodes and links for the variables.

(label-nema
 (add-nema
  (label2uid "ground")
  (label2uid "ground")
  nil)
 "main-vars")

;; Argument list of node 1 (points to "main-vars", which contains nil)
;; In order words, List 1 doesn't have any unbound variables.

(label-nema
 (add-nema
  (label2uid "main-vars")
  (label2uid "main-prog")
  'var)
 "main-args")

(label-nema
 (add-nema
  (label2uid "ground")
  (label2uid "ground")
  '(x))
 "sub-vars")

;; Argument lists of node 2 and node 3

(label-nema
 (add-nema
  (label2uid "sub-vars")
  (label2uid "sub-foo")
  'var)
 "foo-args")

(label-nema
 (add-nema
  (label2uid "sub-vars")
  (label2uid "sub-goo")
  'var)
 "goo-args")

; We provide functions to identify scholia of type
;; var and sub and retrieve the appropriate data.

(defun get-dependencies (art)
  (mapcar '(lambda (x)
	     (list (cadr (get-content x))
		   (get-sink x)))
	  (filter
	   '(lambda (y)
	      (equal 'sub 
		     (car (get-content y))))
	   (get-forward-links art))))

(defun get-vars (art)
  (delete-dups
   (apply 'append
	  (mapcar 
	   '(lambda (x)
	      (get-content (get-source x)))
	   (filter
	    '(lambda (y)
	       (equal 'var (get-content y)))
	    (get-backward-links art))))))

;; Here is the output they produce:

(get-dependencies (label2uid "main-prog"))
  =>
((goo 4) (foo 3))

(get-vars  (label2uid "sub-foo"))
  =>
(x)

;; Using these functions, we evaluate our node.  Remember that the code
;; at node 1 is supposed to invoke foo and goo, which are found as
;; scholia attached to node 2.  As we see, it does this correctly.

(funcall (node-fun
	  (label2uid "main-prog")
	  'get-content
	  'get-vars 
	  'get-dependencies))
=> (25 9)

;; In case you're interested, here are the gory details of how
;; node-fun wrapped up the code inside the node.

(node-fun (label2uid "main-prog")
	  'get-content
	  'get-vars 
	  'get-dependencies)


(lambda nil (flet ((goo (x) (apply (node-fun 4 (quote get-content) (quote get-vars) (quote get-dependencies)) (list x))) (foo (x) (apply (node-fun 3 (quote get-content) (quote get-vars) (quote get-dependencies)) (list x)))) (list (foo 5) (goo 6))))


  => 
(lambda nil (flet ((goo (x) (apply (node-fun 3
					     (quote get-txt)
					     (quote get-vars)
					     (quote get-dependencies))
				   (list x)))
		   (foo (x) (apply (node-fun 2
					     (quote get-txt)
					     (quote get-vars)
					     (quote get-dependencies))
				   (list x))))
	      (list (foo 5) (goo 6))))


(node-fun (label2uid "sub-goo")
	  'get-content
	  'get-vars 
	  'get-dependencies)
=>
(lambda (x) (flet nil (+ x 3)))

(get-content (label2uid "sub-goo")) => ((+ x 3))
(get-vars (label2uid "sub-goo")) => (x)
(get-dependencies (label2uid "sub-goo")) => nil

