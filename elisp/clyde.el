;; clyde.el - Example of scholium seach using a classic semantic network 

;; Copyright (C) 2013 Raymond S. Puzio

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; COMMENTARY:

;; See http://arxana.net/semnet.jpg for a vital map.

;;; CODE:

(load-file "bootstart.el")
(load-file "search.expanded.el")

;; We enter the semantic net in node by node.

;; Start with "ground"
(setq article-list '(1 (0 (0) () (0))))
 =>
(1 (0 (0) nil (0)))

(ins-nod (get-gnd) 'CLYDE (get-gnd))
 => 1

(ins-nod (get-gnd) 'ROBIN (get-gnd))
 => 2

(ins-nod (get-gnd) 'BIRD (get-gnd))
 => 3

(ins-nod (get-gnd) 'OWN1 (get-gnd))
 => 4

(ins-nod (get-gnd) 'NEST1 (get-gnd))
 => 5

(ins-nod (get-gnd) 'OWN (get-gnd))
 => 6

(ins-nod (get-gnd) 'OWNERSHIP (get-gnd))
 => 7

(ins-nod (get-gnd) 'SITUATION (get-gnd))
 => 8

(ins-nod 1 'isa 2)
 => 9

(ins-nod 2 'isa 3)
 => 10

(ins-nod 4 'owner 1)
 => 11

(ins-nod 4 'ownee 5)
 => 12

(ins-nod 5 'isa 6)
 => 13

(ins-nod 4 'isa 7)
 => 14

(ins-nod 7 'isa 8)
 => 15

;; each of the components -- whether nodes or links -- is represented using a similar syntax
;; 16 at the start of the list represents the next available identifier

article-list
  =>
(16 (15 (7) isa (8))
    (14 (4) isa (7))
    (13 (5) isa (6))
    (12 (4) ownee (5))
    (11 (4) owner (1))
    (10 (2) isa (3))
    (9 (1) isa (2))
    (8 (0) SITUATION (0 15))
    (7 (0 15) OWNERSHIP (0 14))
    (6 (0) OWN (0 13))
    (5 (0 13) NEST1 (0 12))
    (4 (0 14 12 11) OWN1 (0))
    (3 (0) BIRD (0 10))
    (2 (0 10) ROBIN (0 9))
    (1 (0 9) CLYDE (0 11))
    (0 (0 8 7 6 5 4 3 2 1) nil (0 8 7 6 5 4 3 2 1)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; What sort of entity can something be in this example? (I.e. what kind of nodes do `isa' links point to?)
;; This `lambda' makes a predicate on the fly.
;; Send Ray a Trello screenshot for a use case
(search '(((a (lambda (x)
		       (equal x 'isa)))
	      (b))
	    ((b snk a))))
  =>
(((b 2) (a 9))
 ((b 3) (a 10))
 ((b 6) (a 13))
 ((b 7) (a 14))
 ((b 8) (a 15)))

(mapcar (lambda (x)
	    (eval `(let ,x
		        (get-txt b))))
	(search '(((a (lambda (x)
			(equal x 'isa)))
		      (b))
		    ((b snk a)))))
 =>
(ROBIN BIRD OWN OWNERSHIP SITUATION)

;; What does Clyde own?
;; (This could be written [clyde owns ?] but the macro expansion language would have to know that ownership is stored in two parts here.)

(search '(((a (lambda (x) (equal x 'CLYDE)))
	      (b (lambda (x) (equal x 'owner)))
	         (c) 
		    (d (lambda (x) (equal x 'ownee)))
		       (e)
		          (f (lambda (x) (equal x 'isa)))
			     (g (lambda (x) (equal x 'OWNERSHIP))))
	    ((a snk b)
	        (c src d)
		   (c src b)
		      (e snk d)
		         (c src f)
			    (g snk f))))
  =>
(((g 7) (f 14) (e 5) (d 12) (c 4) (b 11) (a 1)))

(mapcar '(lambda (x)
	      (eval `(let ,x
		           (get-txt e))))
	(search '(((a (lambda (x) (equal x 'CLYDE)))
		      (b (lambda (x) (equal x 'owner)))
		         (c) 
			    (d (lambda (x) (equal x 'ownee)))
			       (e)
			          (f (lambda (x) (equal x 'isa)))
				     (g (lambda (x) (equal x 'OWNERSHIP))))
		    ((a snk b)
		        (c src d)
			   (c src b)
			      (e snk d)
			         (c src f)
				    (g snk f)))))
  => (NEST1)

