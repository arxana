;; honey.el - Higher Order NEsted Yarnknotter --- back-end for Arxana 

;; Copyright (C) 2010, 2011, 2012, 2013 Raymond S. Puzio

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; See honey-demo.tex for background.

;;; Code:

(defun new-net (parent)
  "Create a new network."
  (let ((newbie (list '*network*
                      1                               ; article counter
                      (make-hash-table :test 'equal)  ; article table
                      (make-hash-table :test 'equal)  ; forward links
                      (make-hash-table :test 'equal)  ; backward links
                      (make-hash-table :test 'equal)  ; forward labels
                      (make-hash-table :test 'equal)  ; backward labels
                      parent)))
                                        ; Define ground and article-type.
    (puthash 0 '(0 0) (nth 2 newbie))
    (puthash 1 '(0 0) (nth 2 newbie))
    (puthash 0 '((0 . 0) (1 . 0)) (nth 3 newbie))
    (puthash 0 '((0 . 0) (1 . 0)) (nth 4 newbie))
    (puthash 0 '"ground" (nth 5 newbie))
    (puthash '"ground" 0 (nth 6 newbie))
    (puthash 1 '"article-type" (nth 5 newbie))
    (puthash '"article-type" 1 (nth 6 newbie))
    newbie))

(defvar root-level 0)

(defun set-net (net)
  "Examine a different network instead."
  (setq current-net net))

(defmacro with-current-net (net &rest expr)
  (append `(let ((current-net ,net))) ,expr))

(defun current-net ()
  "Return the network currently being examined."
  current-net)

(defun reset-net ()
  "Reset the database to its initial configuration."
  ; Reset article counter and hash tables.
  (setcar (cdr current-net) 1)
  (dotimes (n 5) (clrhash (nth (+ n 2) current-net)))
  ; Define ground and article-type.
  (puthash 0 '(0 0) (nth 2 current-net))
  (puthash 1 '(0 0) (nth 2 current-net))
  (puthash 0 '((0 . 0) (1 . 0)) (nth 3 current-net))
  (puthash 0 '((0 . 0) (1 . 0)) (nth 4 current-net))
  (puthash 0 '"ground" (nth 5 current-net))
  (puthash '"ground" 0 (nth 6 current-net))
  (puthash 1 '"article-type" (nth 5 current-net))
  (puthash '"article-type" 1 (nth 6 current-net))
  nil)

(defun next-unique-id ()
  "Produce a yet unused unique identifier."
  (setcar (cdr current-net)
	    (1+ (cadr current-net))))

(defun download-en-masse ()
  "Produce a representation of the database as quintuples."
  (let ((net nil))
    (maphash '(lambda (uid tplt)
		; Unpack triplet.
		(let ((src (car tplt))
		            (snk (nth 1 tplt))
			          (txt (nthcdr 2 tplt)))
		    ; Obtain next label if exists.
		    (setq lbl (gethash uid
				            (nth 5 current-net)
					         nil))
		      ; Write data to list.
		      (setq net (cons `(,uid ,lbl ,src ,snk . ,txt) 
				        net))))
	          (nth 2 current-net))
    ; Return list of data.
    (reverse net)))

(defun upload-en-masse (net)
  "Load a representation of a database as quintuples into memory."
  (dolist (qplt net t)
    ; unpack quintuplet
    (let ((uid (car qplt))
	    (lbl (nth 1 qplt))
	      (src (nth 2 qplt))
	        (snk (nth 3 qplt))
		  (txt (nthcdr 4 qplt)))
      ; plug into tables
      (puthash uid 
	              `(,src ,snk . ,txt)
		             (nth 2 current-net))
      (puthash src
	              (cons `(,uid . ,snk)
			         (gethash src (nth 3 current-net) nil))
		             (nth 3 current-net))
      (puthash snk
	              (cons
		       `(,uid . ,src)
		       (gethash snk (nth 4 current-net) nil))
		             (nth 4 current-net))
      (when lbl
	  (progn
	        (puthash uid lbl (nth 5 current-net))
		    (puthash lbl uid (nth 6 current-net))))
      ; Bump up article counter if needed.
      (when (> uid (cadr current-net))
	     (setcar (cdr current-net) uid)))))

(defun add-en-masse (net)
  "Add multiple articles given as list of quartuplets."
  (mapcar (lambda (qplt)
	    (let ((uid next-unique-id))
	      (put-article (nth 1 net)
			   (nth 2 net)
			   (nthcar 2 net))
	      (label-article uid (car qplt))))
	  net))

(defun put-article (src snk txt)
  "Enter a new article to the database."
  (let ((uid (next-unique-id)))
    ; Add record to article table.
    (puthash uid
	           `(,src ,snk . ,txt)
		         (nth 2 current-net))
    ; Add record to list of forward links of source.
    (puthash src
	          (cons `(,uid . ,snk)
			      (gethash src (nth 3 current-net) nil))
		       (nth 3 current-net))
    ; Add record to list of backward links of sink.
    (puthash snk
	          (cons
		         `(,uid . ,src)
			       (gethash snk (nth 4 current-net) nil))
		       (nth 4 current-net))
    ; Return the id of the new article.
    uid))

(defun get-content (uid)
  "Return the content of the article."
  (cddr (gethash uid (nth 2 current-net))))

(defun get-source (uid)
  "Return the source of the article."
  (car (gethash uid (nth 2 current-net))))

(defun get-sink (uid)
  "Return the sink of the article."
  (cadr (gethash uid (nth 2 current-net))))

(defun update-content (uid txt)
  "Replace the content of the article."
  (puthash uid
	      (let ((x (gethash uid (nth 2 current-net))))
		     `(,(car x)    ; old source
		              ,(cadr x) . ; old sink
			             ,txt))      ; new content
	         (nth 2 current-net)))

(defun update-source (uid new-src)
  "Replace the source of the article."
  (let* ((x (gethash uid (nth 2 current-net)))
	  (old-src (car x))   ; extract current source
	   (old-snk (cadr x))  ; extract current sink 
	    (old-txt (cddr x))) ; extract current content 
    ; Update the entry in the article table.
    (puthash uid
	          `(,new-src ,old-snk . ,old-txt)
		       (nth 2 current-net))
    ; Remove the entry with the old source in the
    ; forward link table.  If that is the only entry
    ; filed under old-src, remove it from table.
    (let ((y (delete `(,uid . ,old-snk)
		          (gethash old-src (nth 3 current-net) nil))))
      (if y
	    (puthash old-src y (nth 3 current-net))
	(remhash old-src (nth 3 current-net))))
    ; Add an entry with the new source in the
    ; forward link table.
    (puthash new-src 
	          (cons `(,uid . ,old-snk)
			   (gethash old-src (nth 3 current-net) nil))
		       (nth 3 current-net))
    ; Update the entry in the backward link table.
    (puthash old-snk
	          (cons `(,uid . ,new-src)
			   (delete `(,uid . ,old-src)
				      (gethash old-src (nth 4 current-net) nil)))
		       (nth 4 current-net))))

(defun update-sink (uid new-snk)
  "Change the sink of the article."
  (let* ((x (gethash uid (nth 2 current-net)))
	  (old-src (car x))   ; extract current source
	   (old-snk (cadr x))  ; extract current sink 
	    (old-txt (cddr x))) ; extract current content 
    ; Update the entry in the article table.
    (puthash uid
	          `(,old-src ,new-snk . ,old-txt)
		       (nth 2 current-net))
    ; Remove the entry with the old sink in the
    ; backward link table.  If that is the only entry
    ; filed under old-src, remove it from table.
    (let ((y (delete `(,uid . ,old-src)
		          (gethash old-snk (nth 4 current-net) nil))))
      (if y
	    (puthash old-snk y (nth 4 current-net))
	(remhash old-snk (nth 4 current-net))))
    ; Add an entry with the new source in the
    ; backward link table.
    (puthash new-snk 
	          (cons `(,uid . ,old-src)
			   (gethash old-snk (nth 4 current-net) nil))
		       (nth 4 current-net))
    ; Update the entry in the forward link table.
    (puthash old-src
	          (cons `(,uid . ,new-snk)
			   (delete `(,uid . ,old-snk)
				      (gethash old-src (nth 3 current-net) nil)))
		       (nth 3 current-net))))

(defun remove-article (uid)
  "Remove this article from the database."
  (let ((old-src (car (gethash uid (nth 2 current-net))))
	(old-snk (cadr (gethash uid (nth 2 current-net)))))
  ; Remove forward link created by article.
  (let ((new-fwd (delete `(,uid . ,old-snk)
			  (gethash old-src (nth 3 current-net)))))
    (if new-fwd 
	(puthash old-src new-fwd (nth 3 current-net))
      (remhash old-src (nth 3 current-net))))
  ; Remove backward link created by article.
  (let ((new-bkw (delete `(,uid . ,old-src)
			  (gethash old-snk (nth 4 current-net)))))
    (if new-bkw
	(puthash old-snk new-bkw (nth 4 current-net))
      (remhash old-snk (nth 4 current-net))))
  ; Remove record from article table.
  (remhash uid (nth 2 current-net))))

(defun get-forward-links (uid)
  "Return all links having given object as source."
  (mapcar 'car (gethash uid (nth 3 current-net))))

(defun get-backward-links (uid)
  "Return all links having given object as sink."
  (mapcar 'car (gethash uid (nth 4 current-net))))

(defun label-article (uid label)
  "Assign the label to the given object."
  (puthash uid label (nth 5 current-net))
  (puthash label uid (nth 6 current-net)))

(defun label2uid (label)
  "Return the unique identifier corresponding to a label."
  (gethash label (nth 6 current-net) nil))

(defun uid2label (uid)
  "Return the label associated to a unique identifier."
  (gethash uid (nth 5 current-net) nil))

;; Search and Query

(defun uid-p (uid)
  "Is this a valid uid?"
  (let ((z '(())))
    (not (eq z (gethash uid (nth 2 current-net) z)))))

(defun ground-p (uid)
  "Is this article the ground?"
  (= uid 0))

(defun source-p (x y)
  "Is the former article the sink of the latter?" 
  (equal x (get-source y)))
     
(defun sink-p (x y)
  "Is the former article the sink of the latter?" 
  (equal x (get-sink y)))

(defun links-from (x y)
  "Return all links from article x to artice y."
  (filter '(lambda (z) (source-p x z))
	    (get-backward-links y)))

(defun links-p (x y)
  "Does article x link to article y?"
  (when (member x (mapcar 
		      'get-source 
		         (get-backward-links y))) t))

(defun triple-p (x y z)
  "Do the three articles form a triplet?"
  (and (source-p y x)
       (sink-p y z)))

(defun network-p (x)
  "Is this object a network?"
  (let ((ans t))
    (setq ans (and ans 
		      (equal (car x) "*network*")))
    (setq ans (and ans
		      (integrp (cadr x))))
    (dotimes (n 5)
          (setq ans (and ans (hash-table-p 
			            (nth (+ n 2) x)))))
    ans)) 

;; Upgrade this to concatenate the results together.
;; Also maybe allow options to add headers or to
;; only loop over unique tuplets.

(defmacro search (vars prop)
  "Find all n-tuplets satisfying a condition"
  ;; Surround the search within dolist loops on free variables.
  (let ((foo '(lambda (vars cmnd)
		(if vars
		        Wrap in a loop.
		        `(dolist (,(car vars) uids)
			          ,(funcall foo (cdr vars) cmnd))
			;; Wrap no further when finished.
		    cmnd))))
    (funcall foo vars prop)))

(provide 'honey)
