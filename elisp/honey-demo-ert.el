(load-file "honey-redux.el")

(ert-deftest honey-demo-tests ()
  (should
   (equal
    (car (set-current-plexus (add-plexus)))
    '*plexus*))
  (should
   (equal
    (add-nema 0 0 "walrus")
    2))
  (should
   (equal
    (add-nema 0 0 "mammal")
    3))
  (should
   (equal
    (add-nema 2 3 nil)
    4))
  (should
   (equal
    (label-nema 2 "walrus")
    2))
  (should
   (equal
    (label-nema 3 "mammal")
    3))
  (should
   (equal
    (label2uid "mammal")
    3))
  (should
   (equal
    (uid2label 2)
    "walrus"))
  (should
   (equal
    (get-content 2)
    "walrus"))
  (should
   (equal
    (get-source 4)
    2))
  (should
   (equal
    (get-sink 4)
    3))
  (should
   (equal
    (uid2label (get-sink 4))
    "mammal"))
  (should
   (equal
    (get-content (label2uid "walrus"))
    "walrus"))
  (should
   (equal
    (setq simple-example (download-en-masse))
    '((0 "ground" 0 0)
      (1 "type" 0 0)
      (2 "walrus" 0 0 . "walrus")
      (3 "mammal" 0 0 . "mammal")
      (4 nil 2 3))))
  (should
   (equal
    (reset-plexus)
    nil))
  (should
   (equal
    (download-en-masse)
    '((0 "ground" 0 0)
      (1 "nema-type" 0 0))))
  (should
   (equal
    (upload-en-masse simple-example)
    t))
  (should
   (equal
    (uid2label (get-source 4))
    "walrus"))
  (should
   (equal
    simple-example
    (download-en-masse)))
  (should
   (equal
    (label-nema
     (add-nema (label2uid "type")
	       (label2uid "ground")
	       nil)
     "isa-type")
    5))
  (should
   (equal
    (label-nema
     (add-nema (label2uid "type")
	       (label2uid "ground")
	       nil)
     "thing-type")
    6))
  (should
   (equal
    (uid2label (get-source 5))
    "type"))
  (should
   (equal
    (uid2label (get-sink 5))
    "ground"))
  (should
   (equal
    (remove-nema 5)
    nil))
  (should
   (equal
    (label-nema
     (add-nema (label2uid "ground")
	       (label2uid "type")
	       "isa")
     "isa-type")
    7))
  (should
   (equal
    (update-source 6 (label2uid "ground"))
    '((6 . 0)(7 . 0))))
  (should
   (equal
    (update-sink 6 (label2uid "type"))
    '((6 . 1))))
  (should
   (equal
    (update-content 6 "thing")
    '(0 1 . "thing")))
  (should
   (equal
    (add-nema (label2uid "thing-type")
		 (label2uid "walrus")
		 nil)
    8))
  (should
   (equal
    (add-nema (label2uid "thing-type")
	      (label2uid "mammal")
	      nil)
    9))
  (should
   (equal
    (add-nema (label2uid "isa-type")
	      4
	      nil)
    10)))



