;; binom-demo.el - Example of scholium programming using a math example

;; Copyright (C) 2013 Raymond S. Puzio

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; COMMENTARY:

;; See http://arxana.net/binom.jpg for a vital map.

;;; CODE:

(load-file "prelim.el")
(load-file "bootstart.el")
(load-file "schprog.el")

;; Access functions to pull up relevant information
;; for operations to weave, tangle, and run the program. 

(defun get-dependencies (art)
  (mapcar '(lambda (x)
	     (list (cadr (get-txt x))
		   (get-snk x)))
	  (filter
	   '(lambda (y)
	      (let ((z (get-txt y)))
		(when (listp z)
		  (equal 'sub
			 (car z)))))
	      (get-flk art))))

(defun get-vars (art)
  (delete-dups
   (apply 'append
	  (mapcar 
	   '(lambda (x)
	      (get-txt (get-src x)))
	   (filter
	    '(lambda (y)
	       (equal 'var (get-txt y)))
	    (get-blk art))))))

(defun get-name (art)
  (get-txt
   (get-src
    (car
     (filter
      '(lambda (y)
	 (equal 'name (get-txt y)))
      (get-blk art))))))

(defun get-chunks (art)
  (mapcar '(lambda (x)
	     (list (cadr (get-txt x))
		   (get-src x)))
	  (filter
	   '(lambda (y)
	      (let ((z (get-txt y)))
		(when (listp z)
		  (equal 'chunk 
			 (car z)))))
	   (get-blk art))))

(defun get-code (node)
  (tangle-module node
		 'get-txt 
		 'get-chunks))

;; The network shown in the accompanying picture.

(set 'article-list
     '(24 (23 (5) (chunk (CALL MYSELF)) (3))
	  (22 (4) (chunk (BOUNDARY CASE)) (3))
	  (21 (11) var (3))
	  (20 (10) name (3))
	  (19 (9) var (2))
	  (18 (8) name (2))
	  (17 (3) (sub choose) (3))
	  (16 (2) (sub pow) (2))
	  (15 (1) (sub choose) (3))
	  (14 (1) (sub pow) (2))
	  (13 (7) var (1))
	  (12 (6) name (1))
	  (11 (0 21) (m n) (0))
	  (10 (0 20) choose (0))
	  (9 (0 19) (x n) (0))
	  (8 (0 18) pow (0))
	  (7 (0 13) (m n p) (0))
	  (6 (0 12) bin-prob (0))
	  (5 (0 23) 
	     (+ (choose (- m 1) n)
		(choose (- m 1) (- n 1)))
	     (0))
	  (4 (0 22)
	     (or (equal n 0)
		 (equal n m))
	     (0))
	  (3 (0 17) 
	     ((if (BOUNDARY CASE)
		  1
		(CALL MYSELF)))
	     (0 23 22 21 20 17 15))
	  (2 (0 16)
	     ((if (equal n 0)
		  1
		(* x (pow x (- n 1)))))
	     (0 19 18 16 14))
	  (1 (0 15 14)
	     ((* (choose m n)
		 (pow (- 1 p) (- n 1)) 
		 (pow p n)))
	     (0 13 12))
	  (0 (0 11 10 9 8 7 6 5 4 3 2 1)
	     nil
	     (0 11 10 9 8 7 6 5 4 3 2 1))))

;; Run the program in situ. 

(funcall (node-fun 1
		   'get-code
		   'get-vars
		   'get-dependencies)
	 3 2 0.5)
=> 0.375

;; A look under the hood.

(node-fun 1
	  'get-code
	  'get-vars
	  'get-dependencies)
  =>
(lambda (m n) 
  (flet ((choose (m n)
		 (apply (node-fun 1 
				  (quote get-code)
				  (quote get-vars)
				  (quote get-dependencies))
			(list m n))))
    (if (or (equal n 0)
	    (equal n m)) 
	1 
      (+ (choose (- m 1) n)
	 (choose (- m 1) (- n 1))))))

;;  Tangle it down to a regular program lising, then run it.

(tangle-routine 1
		'get-name
		'get-vars
		'get-code
		'get-chunks
		'get-dependencies)
  =>
(defun bin-prob (m n p) 
  (* (choose m n)
     (pow (- 1 p) (- n 1))
     (pow p n)))
=> bin-prob

(tangle-routine 2
		'get-name
		'get-vars
		'get-code
		'get-chunks
		'get-dependencies)
  =>
(defun pow (x n) (if (equal n 0) 
		     1 
		   (* x (pow x (- n 1)))))

=> pow

(tangle-routine 3
		'get-name
		'get-vars
		'get-code
		'get-chunks
		'get-dependencies)
  =>
(defun choose (m n)
  (if (or (equal n 0)
	  (equal n m))
      1 (+ (choose (- m 1) n)
	   (choose (- m 1) (- n 1)))))
=> choose

(bin-prob 3 2 0.5)
=> 0.375






