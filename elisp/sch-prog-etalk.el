;; Demonstration of scholiumific programming

;; Define the utility for running code.

(defun node-fun (node retrieve-code 
		      retrieve-vars
		      retrieve-link)
  (list (quote lambda)
	(funcall retrieve-vars node)
	(cons (quote flet)
	      (cons
	       (mapcar
		(lambda (item)
		  (let ((var-list
			 (funcall retrieve-vars (cadr item))))
		    (\` ((\, (car item))
			 (\, var-list)
			 (apply (node-fun (\, (cadr item)) 
					  (quote (\, retrieve-code))
					  (quote (\, retrieve-vars))
					  (quote (\, retrieve-link)))
				(\, (cons (quote list) var-list))))))) 
		(funcall retrieve-link node))
	       (funcall retrieve-code node)))))

;; Make a new scholium-based document.

(set-current-plexus (add-plexus))

;; Place the program in the document.

(upload-en-masse
 '((0 "ground" 0 0)
   (1 "type" 0 0)
   (2 "main-prog" 0 0 (list (foo 5) (goo 6)))
   (3 "sub-foo" 0 0 (* x x))
   (4 "sub-goo" 0 0 (+ x 3))
   (5 "link-foo" 2 3 sub foo)
   (6 "link-goo" 2 4 sub goo)
   (7 "main-vars" 0 0)
   (8 "main-args" 7 2 . var)
   (9 "sub-vars" 0 0 x)
   (10 "foo-args" 9 3 . var)
   (11 "goo-args" 9 4 . var)))

; We provide functions to identify scholia of type
;; var and sub and retrieve the appropriate data.

(defun get-dependencies (art)
  (mapcar '(lambda (x)
	     (list (cadr (get-content x))
		   (get-sink x)))
	  (filter
	   '(lambda (y)
	      (equal 'sub 
		     (car (get-content y))))
	   (get-forward-links art))))

(defun get-vars (art)
  (delete-dups
   (apply 'append
	  (mapcar 
	   '(lambda (x)
	      (get-content (get-source x)))
	   (filter
	    '(lambda (y)
	       (equal 'var (get-content y)))
	    (get-backward-links art))))))

;; Here is the output they produce:

(get-dependencies (label2uid "main-prog"))
;;   =>
;; ((goo 4) (foo 3))

(get-vars  (label2uid "sub-foo"))
;;   =>
;; (x)

;; Using these functions, we evaluate our node.  Remember that the code
;; at "main-prog" is supposed to invoke foo and goo, which are found as
;; scholia attached to that node.  As we see, it does this correctly.

(funcall (node-fun
	  (label2uid "main-prog")
	  'get-content
	  'get-vars 
	  'get-dependencies))
;; => (25 9)

;; In case you're interested, here are the gory details of how
;; node-fun wrapped up the code inside the node.

(node-fun (label2uid "main-prog")
	  'get-content
	  'get-vars 
	  'get-dependencies)

;;   => 

;; (lambda nil (flet ((goo (x) (apply (node-fun 4
;; 					     (quote get-content)
;; 					     (quote get-vars)
;; 					     (quote get-dependencies))
;; 				   (list x)))
;; 		   (foo (x) (apply (node-fun 3
;; 					     (quote get-content)
;; 					     (quote get-vars)
;; 					     (quote get-dependencies))
;; 				   (list x))))
;; 	      (list (foo 5) (goo 6)))) 

(node-fun (label2uid "sub-goo")
	  'get-content
	  'get-vars 
	  'get-dependencies)
;; =>
;; (lambda (x) (flet nil (+ x 3)))

(get-content (label2uid "sub-goo")) ; => ((+ x 3))
(get-vars (label2uid "sub-goo")) ; => (x)
(get-dependencies (label2uid "sub-goo")) ; => nil
