;; Not fully tested

(defun triples-given-beginning (node)
  "Get triples outbound from the given NODE."
  (filter-plexus
   (lambda (id)
     (and (equal (get-content (get-source id)) node)
	  (ground-p (get-source (get-source id)))
	  (ground-p (get-source (get-sink id)))
	  (ground-p (get-sink (get-source id)))
	  (ground-p (get-sink (get-sink id)))))))

(defun triples-given-end (node)
  "Get triples inbound into NODE."
  (filter-plexus
   (lambda (id)
     (and (equal (get-content (get-sink id)) node)
	  (ground-p (get-source (get-source id)))
	  (ground-p (get-source (get-sink id)))
	  (ground-p (get-sink (get-source id)))
	  (ground-p (get-sink (get-sink id)))))))

(defun triples-given-middle (edge)
  "Get the triples that run along EDGE."
  (filter-plexus
   (lambda (id)
     (and (equal (get-content id) edge)
	  (ground-p (get-source (get-source id)))
	  (ground-p (get-source (get-sink id)))
	  (ground-p (get-sink (get-source id)))
	  (ground-p (get-sink (get-sink id)))))))

(defun triples-given-middle-and-end (edge node)
  "Get the triples that run along EDGE into NODE."
  (filter-plexus
   (lambda (id)
     (and (equal (get-content id) edge)
	  (equal (get-content (get-sink id) node))
  	  (ground-p (get-source (get-source id)))
	  (ground-p (get-source (get-sink id)))
	  (ground-p (get-sink (get-source id)))
	  (ground-p (get-sink (get-sink id)))))))

(defun triples-given-beginning-and-middle (node edge)
  "Get the triples that run from NODE along EDGE."
  (filter-plexus
   (lambda (id)
     (and (equal (get-content (get-source id)) node)
	  (equal (get-content id) edge)
	  (ground-p (get-source (get-source id)))
	  (ground-p (get-source (get-sink id)))
	  (ground-p (get-sink (get-source id)))
	  (ground-p (get-sink (get-sink id)))))))

(defun triples-given-beginning-and-end (node1 node2)
  "Get the triples that run from NODE1 to NODE2.  Optional
       argument VIEW causes the results to be selected
       into a view with that name."
  (filter-plexus
   (lambda (id)
     (and (equal (get-content (get-source id)) node1)
	  (equal (get-content (get-sink id) node2))
	  (ground-p (get-source (get-source id)))
	  (ground-p (get-source (get-sink id)))
	  (ground-p (get-sink (get-source id)))
	  (ground-p (get-sink (get-sink id)))))))

(defun triple-exact-match (node1 edge node2)
  "Get the triples that run from NODE1 along EDGE to
NODE2."
  (filter-plexus
   (lambda (id)
     (and (equal (get-content (get-source id)) node1)
	  (equal (get-content id) edge)
	  (equal (get-content (get-sink id)) node2)
	  (ground-p (get-source (get-source id)))
	  (ground-p (get-source (get-sink id)))
	  (ground-p (get-sink (get-source id)))
	  (ground-p (get-sink (get-sink id)))))))
