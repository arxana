;; prelim.el - Handy definitions, macros, and functions for hacking arxana or other projects.

;; Copyright (C) 2013 Raymond S. Puzio

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; COMMENTARY:

;;; CODE:

;; Useful utility to filter elements of a list satisfying a condition.

(defun filter (pred stuff)
  (let ((ans nil))
    (dolist (item stuff (reverse ans))
      (if (funcall pred item)
	  (setq ans (cons item ans))
	nil))))

;; (filter '(lambda (x) (= (% x 2) 1)) '(1 2 3 4 5 6 7))
;; => (1 3 5 7)

;; Map and apply rolled into one.

(defun mapply (f l)
  (if (member nil l) nil
    (cons (apply f (mapcar 'car l))
	  (mapply f (mapcar 'cdr l)))))

;;(mapply '+ '((1 2) (3 4)))
;; => (4 6)

;; This is more general than the `intersection' packaged with common lisp

(defun intersection (&rest arg)
  (cond ((null arg) nil)
	((null (cdr arg)) (car arg))
	(t (let ((ans nil))
	     (dolist (elmt (car arg) ans)
	       (let ((remainder (cdr arg)))
		     (while (and remainder 
				 (member elmt (car remainder)))
		       (setq remainder (cdr remainder))
		       (when (null remainder) 
			 (setq ans (cons elmt ans))))))))))

;; (intersection '(a b c d e f g h j)
;; 	         '(a b h j k)
;; 	         '(b d g h j k))
;;   =>
;; (j h b)

;; Substitute for objects in a list.

(defun sublis (sub lis)
  (cond
   ((null lis) nil)
   ((assoc lis sub) (cadr (assoc lis sub)))
   ((atom lis) lis)
   (t (cons (sublis sub (car lis))
	    (sublis sub (cdr lis))))))
  
;; (sublis '((a 1) (b 2) ((1 2 3) c))
;; 	'((a b c) (1 2 3)))
;;   =>
;; ((1 2 c) c)




       
