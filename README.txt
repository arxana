Instructions for accessing and contributing to our “mob”
branch are here: http://bit.ly/13EqYll -- feel free to
join us!

The idea is to refactor pieces of information into
documents in a “holographic” fashion.  This addresses the
idea that a document is made up of paragraphs, a paragraph
is made up of sentences, sentences are made up of words,
and words are made up of letters -- but also that the
world is not as simple and hierarchical as this picture
might make you think.

In Arxana, every object is annotatable. This requires a
different way of thinking about things, but one that is
much more connected to the natural world of process,
rather than the objectified world of static things. We are
inspired by Richard P. Gabriel and Ron Goldman's Mob
Software, and we're enabling a mob branch here on
repo.or.cz as a way to get started.  In the long run we
want to build a more dynamic wiki for programming.

Arxana's source code is available under the terms of the
Affero GNU GPL 3.0. More information on arxana.net.
